def change(soporte,n):
    #Crear tabla para cada valor hasta n
    table = [0] * (n+1)
    table[0] = 1
    for cambio in soporte:
        for i in range(cambio, n + 1):
            #Agregar posibilidades de cambio a cada valor que puede utilizar el cambio dado con las posibilidades anteriores a agregar ese cambio
            table[i] += table[i - cambio]
    print("Se puede dar cambio de $%d de %d maneras"%(n,table[i]))

def numeroValido(n):
    if (n.isdigit()):
        n = int(n)
        if (n>0):
            return True
        else:
            print("Ingrese un número valido")
            return False
    else:
        print("Ingrese un número")
        return False

#Programa principal
soporte = []
#Asignar valores de cambio
num = input("Ingrese un valor de cambio, o escriba 'q' si ya no desea ingresar mas: ")
while(num!="q"):
    while(numeroValido(num)==False):
        num = input()
    num = int(num)
    if (num in soporte):
        print("Numero ya ha sido ingresado")
    else:
        soporte.append(num)
    num = input("Ingrese un valor de cambio, o escriba 'q' si ya no desea ingresar mas: ")
print ("Cambio disponible:",soporte)

#Asignar numero de donde buscar combinaciones
total = input("Ingrese el total de donde se sacarán las combinaciones: ")
while(numeroValido(total)==False):
    total = input()
total = int(total)

change(soporte,total)