def maxCamino(triangulo):
    while ((len(triangulo))>1):
        for i in range(len(triangulo[-1])-1):
            #Encontrar el maximo local empezando del ultimo piso hacia arriba
            maximo = max(triangulo[-1][i],triangulo[-1][i+1])
            triangulo[-2][i] += maximo
        #Remover el ultimo piso hasta que solo quede uno
        triangulo.pop()
    print("El total máximo del tope al fondo es de %d"%triangulo[0][0])

#Programa Principal
triangulo = [[3],[7,4],[2,4,6],[8,5,9,3]]
#Mostrar forma del triangulo
print("Dado el triangulo:")
for piso in triangulo:
    print(piso)
maxCamino(triangulo)