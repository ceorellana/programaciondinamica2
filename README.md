# programacionDinamica2

### Lenguaje utilizado ###
Python 3.6.1

### Uso del repositorio ###
* El archivo coinChange.py corresponde a lo pedido tanto en el literal 1 como en el 2, pues ambos buscan el numero de combinaciones con las que se puede obtener un numero dado, siendo la diferencia que en el literal 2 los valores de cambio son fijos como 3, 5 y 10
	* Se puede modificar como se desee tanto los cambios disponibles como el numero a alcanzar
	* Tomar en cuenta que puede que no exista combinación según el cambio disponible
* El archivo maxTriangle.py obtiene la suma maxima que se puede obtener al ir de la parte de arriba hasta la base del triangulo
	* Se ha representado el triangulo por una lista de listas, siendo cada lista un piso del triangulo
	* Se pueden cambiar los valores y agregar nuevos pisos como se desee, tomando en cuenta que el piso n tiene un elemento mas que el piso n-1